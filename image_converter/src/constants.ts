export enum ImageFormats {
  png = 'png',
  jpg = 'jpg',
  webp = 'webp',
}

export const INITIAL_PATH = 'src/initial_images';
export const CONVERTING_PATH = 'src/converting_images';
