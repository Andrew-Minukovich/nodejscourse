import * as sharp from 'sharp';
import * as fs from 'fs';
import * as path from 'path';
import * as promisesFs from 'fs/promises';
import {IOptions} from './types';
import {getFileOptions} from './utils';

const getImagesFromFolder = async (
  folderName: string,
  initialFormat: string
): Promise<string[]> => {
  try {
    const images = await promisesFs.readdir(folderName);

    return initialFormat
      ? images.filter(img => getFileOptions(img).ext === initialFormat)
      : images;
  } catch (err) {
    console.log(err);
    return [];
  }
};

const convertImages = async (
  initialFolder: string,
  convertingFolder: string,
  convertingFormat: string,
  listImages: string[]
) => {
  try {
    if (!fs.existsSync(convertingFolder)) {
      await promisesFs.mkdir(convertingFolder);
    }

    listImages.forEach((img: string) => {
      fs.readFile(path.join(initialFolder, img), (err, data) => {
        if (err) throw err;

        sharp(data).toFile(
          path.join(
            convertingFolder,
            `${getFileOptions(img).name}.${convertingFormat}`
          )
        );
      });
    });
  } catch (err) {
    console.log(err);
  }
};

export const convertingAction = async (options: IOptions) => {
  const {initialFormat, convertingFormat, initialFolder, convertingFolder} =
    options;

  if (!fs.existsSync(initialFolder)) return;

  const listImages = await getImagesFromFolder(initialFolder, initialFormat);

  listImages.length > 0 &&
    convertImages(
      initialFolder,
      convertingFolder,
      convertingFormat,
      listImages
    );
};
