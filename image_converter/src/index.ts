import {Command} from 'commander';
import {CONVERTING_PATH, ImageFormats, INITIAL_PATH} from './constants';
import {convertingAction} from './converter';

const program = new Command();

program
  .command('convert')
  .description(
    'Specify initial format and folder and converting format and folder'
  )
  .option(
    '--initial-format <format>',
    'Format of converting images',
    `${ImageFormats.png}`
  )
  .option(
    '--converting-format <format>',
    'Format of converting images',
    `${ImageFormats.webp}`
  )
  .option(
    '--initial-folder <path>',
    'Path of initial folder with images',
    INITIAL_PATH
  )
  .option(
    '--converting-folder <path>',
    'Format of converting images',
    CONVERTING_PATH
  )
  .action(convertingAction);

program.parse();
