export interface IOptions {
  initialFormat: string;
  convertingFormat: string;
  initialFolder: string;
  convertingFolder: string;
}
