import * as path from 'path';

export const getFileOptions = (file: string) => path.parse(file);
